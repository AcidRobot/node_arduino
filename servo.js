var five = require('johnny-five')
var board = new five.Board()


board.on('ready', function () {
  var servo1 = new five.Servo(9);


  servo1.sweep();

  this.wait(3000, function() {
    
    servo1.stop();
    servo1.center();
  });


})
